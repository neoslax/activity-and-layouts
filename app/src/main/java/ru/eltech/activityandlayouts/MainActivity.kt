package ru.eltech.activityandlayouts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ru.eltech.activityandlayouts.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.btnProceed.setOnClickListener {
            val intent = Intent(this, InfoActivity::class.java).apply {
                putExtra("lName", binding.etLastName.text.toString())
                putExtra("name", binding.etName.text.toString())
                putExtra("sName", binding.etSecondName.text.toString())
                putExtra("age", binding.etAge.text.toString())
                putExtra("hobbies", binding.etHobbies.text.toString())
            }
            startActivity(intent)
        }
    }
}